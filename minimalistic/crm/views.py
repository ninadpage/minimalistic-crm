import logging

from django.core.exceptions import ValidationError
from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.response import Response

from minimalistic.crm.models import Customer
from minimalistic.crm.serializers import CustomerSerializer


logger = logging.getLogger(__name__)            # For normal logging
auditlogger = logging.getLogger('auditlog')     # For Audit logging


# noinspection PyClassHasNoInit,PyMethodMayBeStatic,PyUnusedLocal
class CustomerFilter(filters.FilterSet):
    """
    Filter for Customers on short address (postal code + house number), phone number or email.
    """
    short_address = filters.CharFilter(name='short_address', method='filter_short_address')
    phone_number = filters.CharFilter(name='phone_number', method='filter_phone_number')

    class Meta:
        model = Customer
        fields = ('short_address', 'phone_number', 'email')

    def filter_short_address(self, queryset, name, value):
        try:
            postal_code, house_number = value.split('-')
        except ValueError:
            raise ValidationError('Invalid short_address: must be <postal_code>-<house_number>.')

        return Customer.objects.filter(addresses__postal_code=postal_code,
                                       addresses__house_number=house_number)

    def filter_phone_number(self, queryset, name, value):
        return Customer.objects.filter(phone_numbers__phone=value)


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CustomerFilter

    def list(self, request, *args, **kwargs):
        auditlogger.info('[AUDITLOG] user: %s, path: %s, method: %s, query: %s', request.user,
                         request.path, request.method, request.query_params)
        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        auditlogger.info('[AUDITLOG] user: %s, path: %s, method: %s', request.user,
                         request.path, request.method)
        return super().retrieve(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        auditlogger.info('[AUDITLOG] user: %s, path: %s, method: %s', request.user,
                         request.path, request.method)
        return super().create(request, *args, **kwargs)

    # update isn't implemented properly, yet


class ProductTypesViewSet(viewsets.ViewSet):
    # noinspection PyMethodMayBeStatic,PyUnusedLocal,PyShadowingBuiltins
    def list(self, request, format=None):
        return Response(['Mobile Phone Subscription', 'Internet', 'Prepaid SIM', 'Device'])
