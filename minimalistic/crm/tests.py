from django.test import TestCase
from freezegun import freeze_time
from rest_framework.test import APITestCase

from minimalistic.crm.models import Address, Customer, Note, PhoneNumber, Product


class CustomerModelTests(TestCase):

    def test_customer_full_name(self):
        c1 = Customer.objects.create(first_name='John', last_name='Doe', date_of_birth='2000-01-01')
        self.assertTrue(c1.full_name, 'John Doe')
        c2 = Customer.objects.create(title='Mr.', first_name='John', last_name='Doe',
                                     date_of_birth='2000-01-01')
        self.assertTrue(c2.full_name, 'Mr. John Doe')

    def test_customer_address_relation(self):
        c = Customer.objects.create(first_name='John', last_name='Doe', date_of_birth='2000-01-01')
        a1 = Address.objects.create(house_number='100', street_name='Amsterdamstraat',
                                    postal_code='1000AA', city='Amsterdam', customer=c)
        a2 = Address.objects.create(house_number='101', street_name='Amsterdamstraat',
                                    postal_code='1000AA', city='Amsterdam', customer=c)
        self.assertEqual(list(c.addresses.all()), [a1, a2])

    def test_customer_phone_number_relation(self):
        c = Customer.objects.create(first_name='John', last_name='Doe', date_of_birth='2000-01-01')
        p1 = PhoneNumber.objects.create(phone='+31687654321', customer=c)
        p2 = PhoneNumber.objects.create(phone='+31612345678', customer=c)
        self.assertEqual(list(c.phone_numbers.all()), [p1, p2])

    def test_customer_product_relation(self):
        c = Customer.objects.create(first_name='John', last_name='Doe', date_of_birth='2000-01-01')
        p1 = Product.objects.create(product_type='Phone', product_attributes='Number: 0612345678',
                                    customer=c)
        p2 = Product.objects.create(product_type='Internet', product_attributes='ADSL 100 Mbps',
                                    customer=c)
        self.assertEqual(list(c.products.all()), [p1, p2])

    def test_notes_relation(self):
        c = Customer.objects.create(first_name='John', last_name='Doe', date_of_birth='2000-01-01')
        n1 = Note.objects.create(note='Called on 2018-03-01', customer=c)
        n2 = Note.objects.create(note='Called on 2018-03-07', customer=c)
        self.assertEqual(list(c.notes.all()), [n1, n2])


class CustomerViewSetTests(APITestCase):

    def test_customers_get(self):
        Customer.objects.create(first_name='John', last_name='Doe', date_of_birth='2000-01-01')
        Customer.objects.create(first_name='Adam', last_name='Fleming', date_of_birth='2000-01-02',
                                gender='Male', email='adam@example.com')
        response = self.client.get('/customers/', format='json')
        self.assertEqual(response.status_code, 200)
        expected = [{'id': 1, 'title': '', 'first_name': 'John', 'last_name': 'Doe',
                     'date_of_birth': '2000-01-01', 'gender': '', 'email': '', 'phone_numbers': [],
                     'addresses': [], 'products': [], 'notes': []},
                    {'id': 2, 'title': '', 'first_name': 'Adam', 'last_name': 'Fleming',
                     'date_of_birth': '2000-01-02', 'gender': 'Male', 'email': 'adam@example.com',
                     'phone_numbers': [], 'addresses': [], 'products': [], 'notes': []}]
        self.assertEqual(response.data, expected)

        response = self.client.get('/customers/1/', format='json')
        self.assertEqual(response.status_code, 200)
        expected = {'id': 1, 'title': '', 'first_name': 'John', 'last_name': 'Doe',
                    'date_of_birth': '2000-01-01', 'gender': '', 'email': '', 'phone_numbers': [],
                    'addresses': [], 'products': [], 'notes': []}
        self.assertEqual(response.data, expected)

    @freeze_time('2018-01-01')
    def test_customers_get_linked_data(self):
        c1 = Customer.objects.create(first_name='John', last_name='Doe', date_of_birth='2000-01-01')
        PhoneNumber.objects.create(phone='0612345678', customer=c1)
        c2 = Customer.objects.create(first_name='Adam', last_name='Fleming',
                                     date_of_birth='2000-01-02',
                                     gender='Male', email='adam@example.com')
        Note.objects.create(note='Yet another note', customer=c2)
        Product.objects.create(product_type='Phone', product_attributes='Number: 0612345678',
                               customer=c2)
        response = self.client.get('/customers/', format='json')
        self.assertEqual(response.status_code, 200)
        expected = [{'id': 1, 'title': '', 'first_name': 'John', 'last_name': 'Doe',
                     'date_of_birth': '2000-01-01', 'gender': '', 'email': '',
                     'phone_numbers': [{'phone': '0612345678'}],
                     'addresses': [], 'products': [], 'notes': []},
                    {'id': 2, 'title': '', 'first_name': 'Adam', 'last_name': 'Fleming',
                     'date_of_birth': '2000-01-02', 'gender': 'Male', 'email': 'adam@example.com',
                     'phone_numbers': [], 'addresses': [], 'products': [
                        {'product_type': 'Phone', 'product_attributes': 'Number: 0612345678',
                         'subscription_date': '2018-01-01'}],
                     'notes': [{'note': 'Yet another note'}]}]
        self.assertEqual(response.data, expected)

    @freeze_time('2018-01-01')
    def test_customer_post(self):
        # Happy flow
        respose = self.client.post(
            '/customers/',
            {'title': 'Mr.', 'first_name': 'John', 'last_name': 'Doe',
             'date_of_birth': '2000-01-01', 'email': 'john@example.com',
             'phone_numbers': [{'phone': '0612345678'}],
             'addresses': [{'house_number': '100', 'street_name': 'S1', 'postal_code': '1111AA',
                            'city': 'C1'}],
             'products': [{'product_type': 'Phone', 'product_attributes': 'Number: 0612345678'}],
             'notes': [{'note': 'Note1'}]
             },
            format='json'
        )
        expected = {
            'id': 1, 'title': 'Mr.', 'first_name': 'John', 'last_name': 'Doe',
            'date_of_birth': '2000-01-01', 'gender': '', 'email': 'john@example.com',
            'phone_numbers': [{'phone': '0612345678'}],
            'addresses': [{'house_number': '100', 'street_name': 'S1',
                           'postal_code': '1111AA', 'city': 'C1'}],
            'products': [
                {'product_type': 'Phone', 'product_attributes': 'Number: 0612345678',
                 'subscription_date': '2018-01-01'}],
            'notes': [{'note': 'Note1'}],
        }
        self.assertEqual(respose.status_code, 201)
        self.assertEqual(respose.data, expected)

    def test_customer_post_validate_email(self):
        respose = self.client.post(
            '/customers/',
            {'title': 'Mr.', 'first_name': 'John', 'last_name': 'Doe',
             'date_of_birth': '2000-01-01', 'email': 'john',
             'phone_numbers': [{'phone': '0612345678'}],
             'addresses': [{'house_number': '100', 'street_name': 'S1', 'postal_code': '1111AA',
                            'city': 'C1'}]},
            format='json'
        )
        self.assertEqual(respose.status_code, 400)
        self.assertEqual(respose.data, {'email': ['Enter a valid email address.']})

    def test_customer_post_validate_phone_number(self):
        respose = self.client.post(
            '/customers/',
            {'title': 'Mr.', 'first_name': 'John', 'last_name': 'Doe',
             'date_of_birth': '2000-01-01', 'email': 'john@example.com',
             'phone_numbers': [{'phone': '1234'}],
             'addresses': [{'house_number': '100', 'street_name': 'S1', 'postal_code': '1111AA',
                            'city': 'C1'}]},
            format='json'
        )
        self.assertEqual(respose.status_code, 400)
        self.assertEqual(respose.data,
                         {'phone_numbers': [{'phone': ['Enter a valid phone number.']}]})

    def test_customer_post_fields_missing(self):
        respose = self.client.post(
            '/customers/',
            {'title': 'Mr.',
             'addresses': [{'house_number': '100', 'street_name': 'S1', 'postal_code': '1111AA',
                            'city': 'C1'}]},
            format='json'
        )
        self.assertEqual(respose.status_code, 400)
        self.assertEqual(respose.data, {
            'first_name': ['This field is required.'],
            'last_name': ['This field is required.'],
            'date_of_birth': ['This field is required.'],
            'email': ['This field is required.'],
        })

    def test_customer_post_address_missing(self):
        response = self.client.post(
            '/customers/',
            {'title': 'Mr.', 'first_name': 'John', 'last_name': 'Doe',
             'date_of_birth': '2000-01-01', 'email': 'john@example.com'},
            format='json'
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data,
                         {'addresses': ['This field is required.']})

        response = self.client.post(
            '/customers/',
            {'title': 'Mr.', 'first_name': 'John', 'last_name': 'Doe',
             'date_of_birth': '2000-01-01', 'email': 'john@example.com',
             'addresses': []},    # empty list
            format='json'
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data,
                         {'addresses': ['At least one address is required.']})


class CustomerFilterTests(APITestCase):

    def setUp(self):
        self.client.post(
            '/customers/',
            {'first_name': 'FN1', 'last_name': 'LN1',
             'date_of_birth': '2000-01-01', 'email': 'one@example.com',
             'phone_numbers': [{'phone': '0612345611'}, {'phone': '0612345612'}],
             'addresses': [{'house_number': '100', 'street_name': 'S1', 'postal_code': '1111AA',
                            'city': 'C1'}]},
            format='json'
        )
        self.client.post(
            '/customers/',
            {'title': 'Mr.', 'first_name': 'FN2', 'last_name': 'NL2',
             'date_of_birth': '2000-01-02', 'email': 'two@example.com',
             'phone_numbers': [{'phone': '0612345621'}, {'phone': '0612345622'}],
             'addresses': [{'house_number': '200', 'street_name': 'S2', 'postal_code': '2222BB',
                            'city': 'C2'}]},
            format='json'
        )
        # Different customer at same address
        self.client.post(
            '/customers/',
            {'first_name': 'FN3', 'last_name': 'LN3',
             'date_of_birth': '2000-01-03', 'email': 'three@example.com',
             'phone_numbers': [{'phone': '0612345631'}],
             'addresses': [{'house_number': '100', 'street_name': 'S1', 'postal_code': '1111AA',
                            'city': 'C1'}]},
            format='json'
        )

    def test_search_by_short_address(self):
        # One result
        response = self.client.get('/customers/?short_address=2222BB-200')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['first_name'], 'FN2')

        # Two results
        response = self.client.get('/customers/?short_address=1111AA-100')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)
        result1, result2 = response.data
        self.assertEqual(result1['first_name'], 'FN1')
        self.assertEqual(result2['first_name'], 'FN3')

        # Zero results
        response = self.client.get('/customers/?short_address=1111BB-100')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)

    def test_search_by_short_address_invalid(self):
        response = self.client.get('/customers/?short_address=2222BB')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data,
                         ['Invalid short_address: must be <postal_code>-<house_number>.'])

        response = self.client.get('/customers/?short_address=2222BB-200-A')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data,
                         ['Invalid short_address: must be <postal_code>-<house_number>.'])

    def test_search_by_phone_number(self):
        response = self.client.get('/customers/?phone_number=0612345631')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['first_name'], 'FN3')

        # Matches one out of two phone numbers
        response = self.client.get('/customers/?phone_number=0612345611')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['first_name'], 'FN1')

        # No matches
        response = self.client.get('/customers/?phone_number=0612345641')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)

    def test_search_by_email(self):
        response = self.client.get('/customers/?email=one@example.com')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['first_name'], 'FN1')

        # No matches
        response = self.client.get('/customers/?email=four@example.com')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)

    def test_search_by_multiple_filters(self):
        response = self.client.get('/customers/?email=one@example.com&phone_number=0612345611')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['first_name'], 'FN1')

        response = self.client.get(
            '/customers/?short_address=1111AA-100&email=one@example.com&phone_number=0612345611')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['first_name'], 'FN1')

        # No matches
        response = self.client.get('/customers/?short_address=1111AA-100&email=two@example.com')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)


class ProductTypesViewSetTests(APITestCase):

    def test_get(self):
        response = self.client.get('/producttypes/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            ['Mobile Phone Subscription', 'Internet', 'Prepaid SIM', 'Device']
        )
