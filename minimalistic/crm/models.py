from django.core.validators import validate_email, RegexValidator
from django.db import models


class Customer(models.Model):
    GENDER_MALE = 'M'
    GENDER_FEMALE = 'M'
    GENDER_OTHER = 'O'
    GENDER_CHOICES = (
        (GENDER_MALE, 'Male'),
        (GENDER_FEMALE, 'Female'),
        (GENDER_OTHER, 'Other'),
    )

    title = models.CharField(max_length=16)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    date_of_birth = models.DateField()
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)

    # For a CRM, it makes sense to treat email address as a unique identifier,
    # hence email is an attribute of Customer instead of a foreign key like `address`
    email = models.CharField(max_length=256, validators=[validate_email])

    # Addresses, however, can be multiple - e.g, when a customer is calling about moving
    # to a new address.
    # `addresses` is back-populated from `Address` model

    # Phone numbers can also be multiple - e.g, an alternative number in case
    # the customer is calling about his/her primary number not working.
    # `phone_numbers` is back-populated from `PhoneNumber` model

    # A customer can have multiple products
    # `products` is back-populated from `Product` model

    # A CRM also needs to support miscellaneous notes about the customer.
    # `notes` is back-populated from `Note` model

    # Full name is combination of title, first name & last name
    @property
    def full_name(self):
        # noinspection PyTypeChecker
        return ' '.join(filter(bool, (self.title, self.first_name, self.last_name)))


class Address(models.Model):
    # Only supports Netherlands address format currently.
    house_number = models.CharField(max_length=32)
    street_name = models.CharField(max_length=256)
    postal_code = models.CharField(max_length=32)
    city = models.CharField(max_length=256)

    customer = models.ForeignKey(Customer, related_name='addresses', on_delete=models.CASCADE)


class PhoneNumber(models.Model):
    # Validated according to E.164 format: https://en.wikipedia.org/wiki/E.164
    phone_regex = RegexValidator(
        regex=r'^\+?\d{9,15}$',
        message='Enter a valid phone number.'
    )
    phone = models.CharField(max_length=16, validators=[phone_regex])

    customer = models.ForeignKey(Customer, related_name='phone_numbers', on_delete=models.CASCADE)


class Product(models.Model):
    """
    The list of products is static - just a list of product types. Hence it is defined directly
    as a view.

    This model for Product & list of Products is rather simplistic. It can be improved by adding
    a Product factory (types) to the database, each with its own attributes, then linking
    the instance of a customer's product to that type. For type-specific attributes on the instance
    of the product, `product_attributes` can be a JSONField in supported databases.
    """
    product_type = models.CharField(max_length=64)          # e.g. Mobile phone
    product_attributes = models.CharField(max_length=256)   # e.g. Mobile phone number
    subscription_date = models.DateField(auto_now=True)     # Date when the product was ordered

    customer = models.ForeignKey(Customer, related_name='products', on_delete=models.CASCADE)


class Note(models.Model):
    note = models.CharField(max_length=2048)

    customer = models.ForeignKey(Customer, related_name='notes', on_delete=models.CASCADE)
