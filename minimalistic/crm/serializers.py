from rest_framework import serializers

from minimalistic.crm.models import Customer, PhoneNumber, Address, Product, Note


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ('house_number', 'street_name', 'postal_code', 'city')
        validators = []  # Remove a default "unique together" constraint


class PhoneNumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhoneNumber
        fields = ('phone',)
        validators = []  # Remove a default "unique together" constraint


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('product_type', 'product_attributes', 'subscription_date')
        extra_kwargs = {'product_attributes': {'required': False}}
        validators = []  # Remove a default "unique together" constraint.


class NoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = ('note',)
        validators = []  # Remove a default "unique together" constraint


class CustomerSerializer(serializers.ModelSerializer):
    addresses = AddressSerializer(many=True)
    phone_numbers = PhoneNumberSerializer(many=True, required=False)
    products = ProductSerializer(many=True, required=False)
    notes = NoteSerializer(many=True, required=False)

    class Meta:
        model = Customer
        fields = ('id', 'title', 'first_name', 'last_name', 'date_of_birth', 'gender', 'email',
                  'phone_numbers', 'addresses', 'products', 'notes')
        extra_kwargs = {
            'title': {'required': False},
            'gender': {'required': False},
        }
        validators = []  # Remove a default "unique together" constraint.

    def validate_addresses(self, value):
        if not value:
            raise serializers.ValidationError('At least one address is required.')
        return value

    def create(self, validated_data):
        addresses_data = validated_data.pop('addresses')
        phone_numbers_data = validated_data.pop('phone_numbers', [])
        products_data = validated_data.pop('products', [])
        notes_data = validated_data.pop('notes', [])

        customer = Customer.objects.create(**validated_data)

        for address_data in addresses_data:
            Address.objects.create(customer=customer, **address_data)
        for phone_number_data in phone_numbers_data:
            PhoneNumber.objects.create(customer=customer, **phone_number_data)
        for product_data in products_data:
            Product.objects.create(customer=customer, **product_data)
        for note_data in notes_data:
            Note.objects.create(customer=customer, **note_data)

        return customer
