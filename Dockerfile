FROM python:3.6

# Use ``tini`` as init process (PID 1) - to take care of reaping zombies
ENV TINI_VERSION v0.17.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini.asc /tini.asc
RUN gpg --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 595E85A6B1B4779EA4DAAEC70B588DFF0527A9B7 \
    && gpg --verify /tini.asc
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

# Install system dependencies
RUN apt-get update && apt-get install -y libxml2

RUN pip install uwsgi tox

# Copy the uWSGI ini file
COPY ./uwsgi.ini /etc/uwsgi/

EXPOSE 8000

RUN mkdir -p /app
WORKDIR /app

# install requirements
COPY requirements.txt ./
RUN pip install -r requirements.txt

# copy the source
COPY . ./

# WARNING: Only do this when the database is ephimeral
# (e.g. a sqlite file saved in container file system, which is lost when container exits)
RUN python manage.py migrate

CMD ["uwsgi", "--ini", "/etc/uwsgi/uwsgi.ini"]
