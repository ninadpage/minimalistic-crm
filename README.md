## A REST API for a minimalistic version of a CRM

The API is implemented with [Django REST Framework](django-rest-framework.org/), my first
attempt at using Django or DRF.

Examples of few operations in CRM and how to use the API for those:

* If customer is not yet on the database, application allows you to create that customer
  with these parameters: last name, first name, date of birth, gender, house number, zip
  code, street name, city, mobile number and email address. (there can be multiple
  customers in same address)

  - Make a POST call to `/customer/`. Look at the [schema](#schema-api-docs) for
    the definition of the payload.

* If customer is already a customer, or it's a customer you just created, application
  allows you to search that customer on database with house number and zip code
  together or with mobile number or with email address.
  
  - Make a GET call to `/customer/`, with following supported query params:
  
    - `short_address`: Postal code followed by house number, separated by hyphen (-).
    
    - `phone_number`
    
    - `email`
    
    - Example: Search by postal code & house number:
    
      `/customer/?short_address=1000AA-101`
    
    - Example: Search by postal code & house number, together with mobile number:
      
      `/customer/?short_address=1000AA-101&phone_number=0612345678`

* After search is done and you select the customer from the search list, you should
  see the customer details which is including whole customer data and product data if he
  has any.
  
  - Above API call will return a list of all customers (along with their products)
    matching with the search query

* If that customer doesn't have any product, there should a product list such as
  mobile phones, then you are able to order this product for that customer.
  
  - Make a GET call to `/products/` to get a list of all available products
  
  - Adding a product to an already existing customer is not supported yet
    (see [limitations](#limitations-future-improvements)), but you can add
    a product to a customer while creating one.


#### Audit trail logs

If you want to see the audit trail (like when and what searched etc.), you
can look at the output of the `auditlog` logger. The logs would look like this:

```
[2018-03-15 10:39:13,356 AUDIT LOG] [AUDITLOG] user: AnonymousUser, path: /customers/, method: GET, query: <QueryDict: {'short_address': ['1111AA-100']}>
[2018-03-15 10:39:13,570 AUDIT LOG] [AUDITLOG] user: AnonymousUser, path: /customers/1/, method: GET
[2018-03-15 10:39:13,180 AUDIT LOG] [AUDITLOG] user: AnonymousUser, path: /customers/, method: POST
[2018-03-15 10:39:13,247 AUDIT LOG] [AUDITLOG] user: AnonymousUser, path: /customers/, method: GET, query: <QueryDict: {'short_address': ['1111AA-100'], 'email': ['one@example.com'], 'phone_number': ['0612345611']}>
```

It currently logs to `stderr`, same as Django and normal application logs. But since
it's a different logger with its own handler, it can be easily configured to output
logs somewhere else, by configuring `LOGGING` in settings.


#### Schema & API Docs

* Start the application & head over to:
  - `http://127.0.0.1:8000/schema/` for the schema definitions
  - `http://127.0.0.1:8000/docs/` for the API Docs


#### Requirements

We use [pip-tools](https://github.com/jazzband/pip-tools) to manage requirements. There are
two sets of requirements - `requirements.in` and `requirements-test.in`, compiled to
`requirements.txt` and `requirements-test.txt` respectively.
  

#### Setup (using Docker)

**Prerequisites**:

- [Docker](https://docs.docker.com/install/)

**Steps**:

1. `git clone https://gitlab.com/ninadpage/minimalistic-crm`

2. `docker build -t minicrm .`

3. Tests, coverage & codestyle checks can be executed with
   [tox](http://tox.readthedocs.io/en/latest/)
   
   `docker run -it --rm minicrm tox`

4. `docker run -it --rm -p 8000:8000 minicrm`

   will start the application at `http://127.0.0.1:8000/` on your host.
   This will be running with [uwsgi](https://uwsgi-docs.readthedocs.io/en/latest/)
   instead of Django's test server - for a more production-like setup.
   

#### Setup (using virtualenv)

**Prerequisites**:

- Python 3 (tested with 3.6)

- [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)

**Steps**:

1. `git clone https://gitlab.com/ninadpage/minimalistic-crm`

2. `virtualenv .venv -p python3 && source .venv/bin/activate`

3. `python manage.py migrate` to init db and run migrations

4. Tests, coverage & codestyle checks can be executed with
   [tox](http://tox.readthedocs.io/en/latest/)
   
   `pip install tox`
   
   `tox`

5. `python manage.py runserver`

   will start the application at `http://127.0.0.1:8000/` on your host -
   using Django's test server.


#### Limitations & Future Improvements

- All the nested fields for customer (phone numbers, addresses, notes, products)
  are read-only (i.e, they cannot be updated once a customer is created). This
  can be fixed by adding some boilerplate code to `CustomerSerializer`, as explained
  [here](http://www.django-rest-framework.org/api-guide/serializers/#writing-update-methods-for-nested-representations).

- Currently the app doesn't support authentication - to keep the steps to get it running
  simple (because we don't have to create a user beforehand).

- Lookups can be optimized by using an in-memory trie for indexing - like I've implemented
  [here](https://github.com/ninadpage/contact-book-python/blob/master/src/contactbook/fast_lookup.py)
  for a similar data model.
